﻿
// PlantsVsZombiesToolDlg.h: 头文件
//

#pragma once
#include <tuple>
#include <vector>


// CPlantsVsZombiesToolDlg 对话框
class CPlantsVsZombiesToolDlg : public CDialogEx
{
// 构造
public:
	CPlantsVsZombiesToolDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PLANTSVSZOMBIESTOOL_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	std::vector<std::tuple<POINT, INT, UINT>>	m_vecPoint;

protected:
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	DECLARE_MESSAGE_MAP()

protected:
	void FillUsage();
	void NoticeCollect();

protected:
	void SymmetricalCollect(HWND hWnd, UINT nMarginBlack, UINT nDistance, UINT nToolHeight);
	void OffsetCollect(HWND hWnd, UINT nMarginBlackLeft, UINT nDistance, UINT nToolHeight);
	void WaterPlants(HWND hWnd);
	void PlayingMusicToPlants(HWND hWnd);
	void WaterPlants2(HWND hWnd, UINT nMarginBlack, UINT nDistance, UINT nToolHeight);
	void PlayingMusicToPlants2(HWND hWnd, UINT nMarginBlack, UINT nDistance, UINT nToolHeight);
};
