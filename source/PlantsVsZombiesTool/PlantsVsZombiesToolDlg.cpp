﻿
// PlantsVsZombiesToolDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "PlantsVsZombiesTool.h"
#include "PlantsVsZombiesToolDlg.h"
#include "afxdialogex.h"
#include "ProcessHelper.h"
#include "KeyboardMouseHelper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma data_seg("Shared")
HWND g_hMainWnd = NULL;
#pragma data_seg()
#pragma comment(linker,"/SECTION:Shared,RWS")

#define DISTANCE					100
#define DISTANCE_PLANT				65
#define LEFT						620
#define RIGHT						1600
#define TOOL_HEIGHT					188
#define GARDEN_TOOL_HEIGHT			128
#define MARGIN_BLACK				240

#define PROCESS_NAME				L"PlantsVsZombies.exe"
#define TEXT_RUNNING				L"运行中"
#define TEXT_STOPPED				L"已停止"

#define HOT_KEY_F1					VK_F1
#define HOT_KEY_F2					VK_F2
#define HOT_KEY_F3					VK_F3
#define HOT_KEY_F4					VK_F4
#define HOT_KEY_F5					VK_F5
#define HOT_KEY_F6					VK_F6

// CPlantsVsZombiesToolDlg 对话框
CPlantsVsZombiesToolDlg::CPlantsVsZombiesToolDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PLANTSVSZOMBIESTOOL_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_vecPoint = { {{445,240},149,8}, {{425,409},155,8}, {{380,590},167,8}, {{380,775},167,8} };
}

void CPlantsVsZombiesToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPlantsVsZombiesToolDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_HOTKEY()
END_MESSAGE_MAP()


// CPlantsVsZombiesToolDlg 消息处理程序
BOOL CPlantsVsZombiesToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	if (NULL != g_hMainWnd)
	{
		::ShowWindow(g_hMainWnd, SW_SHOW);
		PostMessageW(WM_CLOSE);
	}
	else
	{
		g_hMainWnd = m_hWnd;

		HWND hWnd = NULL;
		if (DWORD dwPID = CProcessHelper::GetProcessIdByName(PROCESS_NAME); 0 != dwPID)
		{
			hWnd = CProcessHelper::FindMainWindow(dwPID);
		}
		if (FALSE == IsWindow(hWnd))
		{
			CStringW strMsg;
			strMsg.Format(L"请在启动“植物大战僵尸”游戏(%s)后再运行此程序。", PROCESS_NAME);
			MessageBoxW(strMsg, L"", MB_ICONSTOP | MB_OK);
			PostMessageW(WM_CLOSE);
		}
		else if (FALSE == RegisterHotKey(m_hWnd, 0, 0, HOT_KEY_F1) || FALSE == RegisterHotKey(m_hWnd, 0, 0, HOT_KEY_F2)
			|| FALSE == RegisterHotKey(m_hWnd, 0, 0, HOT_KEY_F3) || FALSE == RegisterHotKey(m_hWnd, 0, 0, HOT_KEY_F4)
			|| FALSE == RegisterHotKey(m_hWnd, 0, 0, HOT_KEY_F5) || FALSE == RegisterHotKey(m_hWnd, 0, 0, HOT_KEY_F6))
		{
			MessageBoxW(L"热键注册失败", L"", MB_ICONSTOP | MB_OK);
			PostMessageW(WM_CLOSE);
		}
		else
		{
			FillUsage();

			WCHAR szTmp[MAX_PATH] = { 0 };
			StringCchPrintfW(szTmp, _countof(szTmp), L"0x%llX", (LONGLONG)hWnd);
			SetDlgItemTextW(IDC_EDT_HWND, szTmp);

			CRect rcWnd;
			::GetWindowRect(hWnd, &rcWnd);
			rcWnd.OffsetRect(-rcWnd.left, -rcWnd.top);
			StringCchPrintfW(szTmp, _countof(szTmp), L"%d", rcWnd.Width());
			SetDlgItemText(IDC_EDT_SCREEN_WIDTH, szTmp);
			StringCchPrintfW(szTmp, _countof(szTmp), L"%d", rcWnd.Height());
			SetDlgItemText(IDC_EDT_SCREEN_HEIGHT, szTmp);

			SetDlgItemInt(IDC_EDT_DISTANCE, DISTANCE);
			SetDlgItemInt(IDC_EDT_LEFT, LEFT);
			SetDlgItemInt(IDC_EDT_RIGHT, RIGHT);

			SetDlgItemInt(IDC_EDT_DISTANCE_PLANTS, DISTANCE_PLANT);
			SetDlgItemInt(IDC_EDT_GARDEN_TOOL_HEIGHT, GARDEN_TOOL_HEIGHT);

			SetDlgItemInt(IDC_EDT_TOOL_HEIGHT, TOOL_HEIGHT);
			SetDlgItemInt(IDC_EDT_MARGIN_BLACK, MARGIN_BLACK);
			ShowWindow(SW_SHOW);
		}
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPlantsVsZombiesToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CPlantsVsZombiesToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPlantsVsZombiesToolDlg::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	WCHAR szTmp[MAX_PATH] = { 0 };
	GetDlgItemTextW(IDC_EDT_HWND, szTmp, _countof(szTmp));
	HWND hWnd = (HWND)wcstoll(szTmp, NULL, 16);
	UINT nMarginLeft = GetDlgItemInt(IDC_EDT_LEFT);
	UINT nMarginBlack = GetDlgItemInt(IDC_EDT_MARGIN_BLACK);
	UINT nDistance = GetDlgItemInt(IDC_EDT_DISTANCE);
	UINT nDistancePlants = GetDlgItemInt(IDC_EDT_DISTANCE_PLANTS);
	UINT nToolHeight = GetDlgItemInt(IDC_EDT_TOOL_HEIGHT);
	UINT nToolHeightGarden = GetDlgItemInt(IDC_EDT_GARDEN_TOOL_HEIGHT);

	switch (nKey2)
	{
	case HOT_KEY_F1:
	{
		OffsetCollect(hWnd, nMarginLeft, nDistance, nToolHeight);
		NoticeCollect();
		break;
	}
	case HOT_KEY_F2:
	{
		SymmetricalCollect(hWnd, nMarginBlack, nDistance, nToolHeight);
		NoticeCollect();
		break;
	}
	case HOT_KEY_F3:
	{
		WaterPlants(hWnd);
		break;
	}
	case HOT_KEY_F4:
	{
		PlayingMusicToPlants(hWnd);
		break;
	}
	case HOT_KEY_F5:
	{
		WaterPlants2(hWnd, nMarginBlack, nDistancePlants, nToolHeightGarden);
		break;
	}
	case HOT_KEY_F6:
	{
		PlayingMusicToPlants2(hWnd, nMarginBlack, nDistancePlants, nToolHeightGarden);
		break;
	}
	default:
	{
		break;
	}
	}

	CDialogEx::OnHotKey(nHotKeyId, nKey1, nKey2);
}

void CPlantsVsZombiesToolDlg::FillUsage()
{
	CListBox* pListBox = (CListBox*)GetDlgItem(IDC_LIST_USAGE);
	if (NULL != pListBox)
	{
		pListBox->AddString(L"F1：捡取除左侧两列外的物品，因为这两列我要放玉米炸弹。");
		pListBox->AddString(L"F2：捡取全屏幕物品");
		pListBox->AddString(L"F3：花园全屏幕浇水(根据坐标)");
		pListBox->AddString(L"F4：花园全屏幕听歌(根据坐标)");
		pListBox->AddString(L"F5：花园全屏幕浇水(根据间隔)");
		pListBox->AddString(L"F6：花园全屏幕听歌(根据间隔)");
	}
}

void CPlantsVsZombiesToolDlg::NoticeCollect()
{
	static UINT n = 0;
	WCHAR szTmp[MAX_PATH] = { 0 };
	wsprintfW(szTmp, L"第%d次收集物品", ++n);
	SetWindowTextW(szTmp);
}

void CPlantsVsZombiesToolDlg::SymmetricalCollect(HWND hWnd, UINT nMarginBlack, UINT nDistance, UINT nToolHeight)
{
	RECT rcWnd = { 0 };
	::GetWindowRect(hWnd, &rcWnd);

	UINT nX = nMarginBlack;
	UINT nY = nToolHeight;
	POINT ptClick = { 0 };
	POINT ptRight = { 0 };
	
	for (ptClick.y = nY; ptClick.y < (rcWnd.bottom + (LONG)nDistance); ptClick.y += nDistance)
	{
		for (ptClick.x = nX; ptClick.x <= (rcWnd.right / 2 + (LONG)nDistance); ptClick.x += nDistance)
		{
			// 做对称拾取，以避免拾取的时候，误使用玉米导弹炸自己(我习惯把玉米导弹放最左侧)。
			CKeyboardMouseHelper::MouseClick(hWnd, &ptClick);
			ptRight = { rcWnd.right - ptClick.x, ptClick.y };
			SwitchToThread();// 让出线程调度供游戏反应，避免过于紧密的点击造成玉米炸弹炸自己。
			CKeyboardMouseHelper::MouseClick(hWnd, &ptRight);
		}
	}
}

void CPlantsVsZombiesToolDlg::OffsetCollect(HWND hWnd, UINT nMarginBlackLeft, UINT nDistance, UINT nToolHeight)
{
	RECT rcWnd = { 0 };
	::GetWindowRect(hWnd, &rcWnd);

	INT nX = nMarginBlackLeft;
	INT nY = nToolHeight;
	POINT ptClick = { 0 };
	POINT ptRight = { 0 };

	for (ptClick.y = nY; ptClick.y < (rcWnd.bottom + (LONG)nDistance); ptClick.y += nDistance)
	{
		for (ptClick.x = nX; ptClick.x <= (rcWnd.right - MARGIN_BLACK); ptClick.x += nDistance)
		{
			// 从左至右从上至下依次拾取
			CKeyboardMouseHelper::MouseClick(hWnd, &ptClick);
		}
	}
}

void CPlantsVsZombiesToolDlg::WaterPlants(HWND hWnd)
{
	POINT ptClick = { 0 };
	POINT ptKettle = { 360,70 };// 水壶的坐标
	for (auto [ptFirst, nDistance, nCount] : m_vecPoint)
	{
		for (LONG i = 0; i < (LONG)nCount; ++i)
		{
			// 每点一次水壶，再点一次植物。
			CKeyboardMouseHelper::MouseClick(hWnd, &ptKettle, true, true);
			Sleep(50);
			ptClick = { ptFirst.x + nDistance * i, ptFirst.y };
			CKeyboardMouseHelper::MouseClick(hWnd, &ptClick, true, true);
			Sleep(50);
		}
	}
}

void CPlantsVsZombiesToolDlg::PlayingMusicToPlants(HWND hWnd)
{
	POINT ptClick = { 0 };
	POINT ptMusicBox = { 735,70 };// 音乐盒的坐标
	for (auto [ptFirst, nDistance, nCount] : m_vecPoint)
	{
		for (LONG i = 0; i < (LONG)nCount; ++i)
		{
			// 每点一次音乐播放器，再点一次植物。
			CKeyboardMouseHelper::MouseClick(hWnd, &ptMusicBox, true, true);
			Sleep(50);
			ptClick = { ptFirst.x + nDistance * i, ptFirst.y };
			CKeyboardMouseHelper::MouseClick(hWnd, &ptClick, true, true);
			Sleep(50);
		}
	}
}

void CPlantsVsZombiesToolDlg::WaterPlants2(HWND hWnd, UINT nMarginBlack, UINT nDistance, UINT nToolHeight)
{
	RECT rcWnd = { 0 };
	::GetWindowRect(hWnd, &rcWnd);

	UINT nX = nMarginBlack;
	UINT nY = nToolHeight + nDistance;
	POINT ptClick = { 0 };
	POINT ptKettle = { 360,70 };// 水壶的坐标

	for (ptClick.y = nY; ptClick.y < (rcWnd.bottom + (LONG)nDistance); ptClick.y += nDistance)
	{
		for (ptClick.x = nX; ptClick.x <= (rcWnd.right - (LONG)nMarginBlack); ptClick.x += nDistance)
		{
			// 从左至右从上至下依次拾取
			CKeyboardMouseHelper::MouseClick(hWnd, &ptKettle, true, true);
			Sleep(50);
			CKeyboardMouseHelper::MouseClick(hWnd, &ptClick, true, true);
			Sleep(50);
		}
	}
}

void CPlantsVsZombiesToolDlg::PlayingMusicToPlants2(HWND hWnd, UINT nMarginBlack, UINT nDistance, UINT nToolHeight)
{
	RECT rcWnd = { 0 };
	::GetWindowRect(hWnd, &rcWnd);

	UINT nX = nMarginBlack;
	UINT nY = nToolHeight + nDistance;
	POINT ptClick = { 0 };
	POINT ptMusicBox = { 735,70 };// 音乐盒的坐标

	for (ptClick.y = nY; ptClick.y < (rcWnd.bottom + (LONG)nDistance); ptClick.y += nDistance)
	{
		for (ptClick.x = nX; ptClick.x <= (rcWnd.right - (LONG)nMarginBlack); ptClick.x += nDistance)
		{
			// 从左至右从上至下依次拾取
			CKeyboardMouseHelper::MouseClick(hWnd, &ptMusicBox, true, true);
			Sleep(50);
			CKeyboardMouseHelper::MouseClick(hWnd, &ptClick, true, true);
			Sleep(50);
		}
	}
}