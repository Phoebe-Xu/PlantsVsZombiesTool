#include "pch.h"
#include "KeyboardMouseHelper.h"

#define TIME_WAITING_INTERVAL		200
void CKeyboardMouseHelper::MouseMove(HWND hWnd, POINT* pOffsetTopLeft /*= nullptr*/)
{
	::SendMessageW(hWnd, WM_MOUSEMOVE, NULL, MAKELPARAM(pOffsetTopLeft->x, pOffsetTopLeft->y));
}

void CKeyboardMouseHelper::MouseClick(HWND hWnd, POINT* pOffsetTopLeft, bool bOnlyMsg /*= true*/, bool bMouseMove /*= false*/)
{
	POINT ptCursor = { 0 };
	GetCursorPos(&ptCursor);
	if (TRUE == IsWindow(hWnd))
	{
		RECT rcWnd = { 0 };
		GetWindowRect(hWnd, &rcWnd);
		ScaleRect(&rcWnd);

		LPARAM lParam = 0;
		POINT pt = { (rcWnd.right - rcWnd.left) / 2, (rcWnd.bottom - rcWnd.top) / 2 };
		if (nullptr != pOffsetTopLeft)
		{
			pt = *pOffsetTopLeft;
		}
		ScalePoint(&pt);
		lParam = MAKELPARAM(pt.x, pt.y);
		if (true == bMouseMove)
		{
			SendMessageW(hWnd, WM_MOUSEMOVE, 0, lParam);
		}

		if (true == bOnlyMsg)
		{
			PostMessageW(hWnd, WM_LBUTTONDOWN, 0, lParam);
			PostMessageW(hWnd, WM_LBUTTONUP, MK_LBUTTON, lParam);
		}
		else
		{
			INPUT input = { 0 };
			input.type = INPUT_MOUSE;
			input.mi.dx = pt.x;
			input.mi.dy = pt.y;
			input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP;
			::SendInput(1, &input, sizeof(INPUT));
		}
	}
	::SetCursorPos(ptCursor.x, ptCursor.y);
}

void CKeyboardMouseHelper::MouseDbClick(HWND hWnd, POINT* pOffsetTopLeft /*= nullptr*/, bool bOnlyMsg /*= true*/)
{
	uint32_t nTime = GetDoubleClickTime();
	MouseClick(hWnd, pOffsetTopLeft, bOnlyMsg);
	Sleep(nTime / 2);
	MouseClick(hWnd, pOffsetTopLeft, bOnlyMsg);
}

void CKeyboardMouseHelper::SetCaption(HWND hWnd, const wchar_t* pszCaption)
{
	if (TRUE == IsWindow(hWnd))
	{
		const wchar_t* pszTmpCaption = (nullptr == pszCaption) ? L"" : pszCaption;
		
		wchar_t szClassName[MAX_PATH] = { 0 };
		GetClassNameW(hWnd, szClassName, _countof(szClassName));

		if (0 == _wcsicmp(L"Edit", szClassName))
		{
			SendMessageW(hWnd, WM_SETTEXT, 0, (LPARAM)pszTmpCaption);
		}
		else if (0 == _wcsicmp(L"AfxWnd42", szClassName))
		{
			::SetFocus(hWnd);
			MouseClick(hWnd, nullptr, false);
			Sleep(TIME_WAITING_INTERVAL);

			size_t nLen = wcslen(pszTmpCaption);
			for (size_t i = 0; i < nLen; ++i)
			{
				PostMessageW(hWnd, WM_CHAR, pszTmpCaption[i], NULL);
				Sleep(TIME_WAITING_INTERVAL);
			}
		}
		else
		{
			SetWindowTextW(hWnd, pszTmpCaption);
		}
	}
}

void CKeyboardMouseHelper::Keyboard(HWND hWnd, const wchar_t* pszCaption)
{
	if (TRUE == IsWindow(hWnd))
	{
		const wchar_t* pszTmpCaption = (nullptr == pszCaption) ? L"" : pszCaption;

		::SetFocus(hWnd);
		MouseClick(hWnd, nullptr, false);
		Sleep(TIME_WAITING_INTERVAL);

		INPUT ip = { 0 };
		ip.type = INPUT_KEYBOARD;
		size_t nLen = wcslen(pszTmpCaption);
		for (size_t i = 0; i < nLen; ++i)
		{
			ip.ki.wVk = pszTmpCaption[i];
			SendInput(1, &ip, sizeof(INPUT));
			Sleep(TIME_WAITING_INTERVAL);
		}
	}
}

bool CKeyboardMouseHelper::SelCombo(HWND hWnd, int nIndex)
{
	bool bSuccess = false;
	WCHAR szClass[MAX_PATH] = { 0 };
	GetClassNameW(hWnd, szClass, _countof(szClass));
	if (0 == _wcsicmp(L"ComboBox", szClass))
	{
		if (false == (bSuccess = (nIndex == SendMessageW(hWnd, CB_GETCURSEL, NULL, NULL))))
		{
			SendMessageW(hWnd, CB_SETCURSEL, nIndex, NULL);
		}
	}
	return bSuccess;
}

double CKeyboardMouseHelper::ScreenScale()
{
	double nScale = 1;
	HWND hWnd = ::GetDesktopWindow();
	if (nullptr != hWnd)
	{
		HDC hDC = ::GetDC(hWnd);
		int nHorizontalDPI = ::GetDeviceCaps(hDC, LOGPIXELSX);
		int nVerticalDPI = ::GetDeviceCaps(hDC, LOGPIXELSY);
		::ReleaseDC(hWnd, hDC);
		hDC = nullptr;

		switch (nHorizontalDPI)
		{
		case 96:	nScale = 1;		break;
		case 120:	nScale = 1.25;	break;
		case 144:	nScale = 1.5;	break;
		case 168:	nScale = 1.75;	break;
		case 192:	nScale = 2;		break;
		default:	break;
		}
	}
	return nScale;
}

void CKeyboardMouseHelper::ScaleRect(RECT* pRect)
{
	if (nullptr != pRect)
	{
		double nScale = ScreenScale();
		pRect->left = (LONG)(pRect->left * nScale);
		pRect->top = (LONG)(pRect->top * nScale);
		pRect->right = (LONG)(pRect->right * nScale);
		pRect->bottom = (LONG)(pRect->bottom * nScale);
	}
}

void CKeyboardMouseHelper::ScalePoint(POINT* pt)
{
	if (nullptr != pt)
	{
		double nScale = ScreenScale();
		pt->x = (LONG)(pt->x * nScale);
		pt->y = (LONG)(pt->y * nScale);
	}
}