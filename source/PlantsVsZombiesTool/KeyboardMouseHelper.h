#pragma once
class CKeyboardMouseHelper
{
public:
	CKeyboardMouseHelper() = delete;
	virtual ~CKeyboardMouseHelper() = delete;

public:
	static void MouseMove(HWND hWnd, POINT* pOffsetTopLeft = nullptr);
	static void MouseClick(HWND hWnd, POINT* pOffsetTopLeft = nullptr, bool bOnlyMsg = true, bool bMouseMove = false);
	static void MouseDbClick(HWND hWnd, POINT* pOffsetTopLeft = nullptr, bool bOnlyMsg = true);

	static void	SetCaption(HWND hWnd, const wchar_t* pszCaption);
	static void	Keyboard(HWND hWnd, const wchar_t* pszCaption);
	static bool SelCombo(HWND hWnd, int nIndex);

public:
	static double ScreenScale();
	static void ScaleRect(RECT* pRect);
	static void ScalePoint(POINT* pt);
};

