#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Tlhelp32.h>

struct HANDLE_DATA
{
	unsigned long process_id;
	HWND best_handle;
};

class CProcessHelper
{
public:
	CProcessHelper() = delete;
	virtual ~CProcessHelper() = delete;

	static DWORD	GetProcessIdByName(LPCWSTR lpszProcessName);
	static HWND		FindMainWindow(DWORD dwPID);

protected:
	static BOOL		EnumWindowsCallback_FindMainWindow(HWND handle, LPARAM lParam);
	static BOOL		IsMainWindow(HWND hWnd, BOOL bVisible = TRUE);
};

