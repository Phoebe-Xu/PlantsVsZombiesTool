﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 PlantsVsZombiesTool.rc 使用
//
#define IDD_PLANTSVSZOMBIESTOOL_DIALOG  102
#define IDR_MAINFRAME                   128
#define IDC_EDT_HWND                    1000
#define IDC_EDT_DISTANCE                1001
#define IDC_EDT_TOOL_HEIGHT             1002
#define IDC_EDT_SCREEN_WIDTH            1003
#define IDC_EDT_SCREEN_HEIGHT           1004
#define IDC_LIST_USAGE                  1006
#define IDC_EDT_LEFT                    1007
#define IDC_EDT_RIGHT                   1008
#define IDC_EDT_DISTANCE_PLANTS         1009
#define IDC_EDT_GARDEN_TOOL_HEIGHT      1010
#define IDC_EDT_MARGIN_BLACK            1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
