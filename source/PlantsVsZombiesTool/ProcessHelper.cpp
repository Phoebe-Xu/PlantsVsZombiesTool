#include "ProcessHelper.h"

DWORD CProcessHelper::GetProcessIdByName(LPCWSTR lpszProcessName)
{
	DWORD dwProcessId = 0;
	HANDLE hSnapshot = NULL;
	__try
	{
		if (NULL == lpszProcessName || L'\0' == lpszProcessName[0])
		{
			__leave;
		}

		hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (INVALID_HANDLE_VALUE == hSnapshot)
		{
			__leave;
		}

		PROCESSENTRY32W pe = { 0 };
		pe.dwSize = sizeof(PROCESSENTRY32W);
		if (FALSE == Process32FirstW(hSnapshot, &pe))
		{
			__leave;
		}

		while (TRUE == Process32NextW(hSnapshot, &pe))
		{
			if (0 == _wcsicmp(pe.szExeFile, lpszProcessName))
			{
				dwProcessId = pe.th32ProcessID;
				__leave;
			}
		}
	}
	__finally
	{
		if (INVALID_HANDLE_VALUE != hSnapshot && NULL != hSnapshot)
		{
			CloseHandle(hSnapshot);
			hSnapshot = NULL;
		}
	}
	return dwProcessId;
}

HWND CProcessHelper::FindMainWindow(DWORD dwPID)
{
	HANDLE_DATA data;
	data.process_id = dwPID;
	data.best_handle = 0;
	EnumWindows(EnumWindowsCallback_FindMainWindow, (LPARAM)&data);
	return data.best_handle;
}

BOOL CProcessHelper::EnumWindowsCallback_FindMainWindow(HWND handle, LPARAM lParam)
{
	HANDLE_DATA& data = *(HANDLE_DATA*)lParam;
	unsigned long process_id = 0;
	GetWindowThreadProcessId(handle, &process_id);
	if (data.process_id != process_id || FALSE == IsMainWindow(handle))
	{
		return TRUE;
	}
	data.best_handle = handle;
	return FALSE;
}

BOOL CProcessHelper::IsMainWindow(HWND hWnd, BOOL bVisible /*= TRUE*/)
{
	BOOL bMainWnd = FALSE;
	if (NULL == GetWindow(hWnd, GW_OWNER) && bVisible == IsWindowVisible(hWnd))
	{
		bMainWnd = TRUE;
	}
	return bMainWnd;
}

